//importing mongoose for easy manupilation of Mongodb
const mongoose = require('mongoose');

//decleration of the schema
const Schema = mongoose.Schema;

//the structure of the schema
const movieSchema = new Schema({
    title:{
        type:String,
        required:[true]
    },
    genre:{
        type:String,
        required:[true]
    },
    country:{
        type:String,
        required:[true]
    },
    studio:{
        type:String,
        required:[true]
    }
});

//creation of the collection in mongodb & schema assignement
const movie = mongoose.model('film',movieSchema);

//exporting the model
module.exports = movie;