//importing express
const express = require('express');

//importing the model
const film = require('../models/db');


//importing router module
const router = express.Router();

//importing validation function from validation module
const validate = require('./validation');

//getting all the data router
router.get('/api', (req,res)=>{
    film.find().then((data)=>{
      res.send(data);
    });
    
});

//getting a particular data by id router
router.get('/api/:id', (req,res)=>{
    film.findById({_id:req.params.id}).then((data)=>{
        res.send(data);
    })
    .catch(()=>{
        res.status(404).send("id given not found");
    })
});

//saving data router
router.post('/api', (req,res)=>{
   const {error} = validate.validateCourse(req.body);
   if(error) return res.status(404).send(error.details[0].message);

   film.create(req.body).then((data)=>{
       res.send(data);
   });   
});

//updating data router
router.put('/api/:id', (req,res)=>{
    const {error} = validate.validateCourse(req.body);
    if(error) return res.status(404).send(error.details[0].message);

    film.findByIdAndUpdate({_id:req.params.id},req.body).then(()=>{
        film.find({_id:req.params.id})
        .then((data)=>{
            res.send(data);
        });   
    })
    .catch(()=>{
        res.status(404).send("id given not found");
    });
});

//deleting data router
router.delete('/api/:id', (req,res)=>{
    film.findByIdAndRemove({_id:req.params.id}).
    then(()=>{
        film.find().then((data)=>{
            res.send(data);
        });
    })
    .catch(()=>{
        res.status(404).send("id given not found");
    });
});

//exporting the router
module.exports = router;