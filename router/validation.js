const Joi = require('joi');


function validateCourse(movie){
    var schema = {
        title: Joi.string().required(),
        genre: Joi.string().min(3).required(),
        country: Joi.string().required(),
        studio: Joi.string().required(),
    };
    return Joi.validate(movie,schema);
}

module.exports.validateCourse = validateCourse;
