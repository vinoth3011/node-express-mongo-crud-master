
// requiring the express, mongoose and route
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const route = require('./router/routing');

//connecting the mongoose to the mongodb with Storage as database name
mongoose.connect('mongodb://192.168.4.87/Storage');
mongoose.Promise = global.Promise;

//for static files
app.use(express.static(__dirname));

//used as bodyParser
app.use(express.json());

//using the route 
app.use(route);


//declaring dynamic port or assign it 3000
const port = process.env.PORT || 3000;

//listening to the port
app.listen(port, ()=>{
    console.log("listening... " +port);
});

