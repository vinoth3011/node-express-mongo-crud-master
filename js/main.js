var app = {
    
    getData: function(){

        $.ajax({
            url:'/api',
            method:'get',
            contentType:'application/json',
            success: successCallBack,
            error: function(error){
                console.log(error);
            }
        });
        
        function successCallBack(data){
            var table = '';
            for(let i=0; i<data.length; i++){
                table +=
                    '<tr>'+
                        '<td class="id" hidden>'+data[i]._id+'</td>'+
                        '<td><input type="text" id="title" class="form-control" value="'+data[i].title+'"></td>'+
                        '<td><input type="text" id="genre" class="form-control" value="'+data[i].genre+'"></td>'+
                        '<td><input type="text" id="country" class="form-control" value="'+data[i].country+'"></td>'+
                        '<td><input type="text" id="studio" class="form-control" value="'+data[i].studio+'"></td>'+
                        '<td><a  class="btn btn-success"><i class="icon-pencil"></i>Edit</a>'+
                        '<a class="btn btn-danger"><i class="icon-trash"></i>Remove</a></td>'+    
                '</tr>';         
            }
            $('tbody').append(table);  

        }

        
    },

    saveData: function(){
        
        $('#save').click((e)=>{
            e.preventDefault();
            var item = {
                title: $('#title').val(),
                genre: $('#genre').val(),
                country: $('#country').val(),
                studio: $('#studio').val(),
            };
            
            $.ajax({
                url:'/api',
                method:'post',
                data: JSON.stringify(item),
                contentType:'application/json',
                success: function(data){    
                    window.location.href= "index.html";
                },
                error: function(error){
                    console.log(error);
                }
            }); 
        });
        
        $('#view').click((e)=>{
            e.preventDefault();
            window.location.href= "index.html";
        })

    },

    updateData: function(){

        $('table').on('click','.btn-success', function(){
            var rowel = $(this).closest('tr');
            var id = rowel.find(".id").text();
            
            var item = {
                title: rowel.find("#title").val(),
                genre: rowel.find("#genre").val(),
                country: rowel.find("#country").val(),
                studio: rowel.find("#studio").val(),
            };
        
            $.ajax({
                url:'/api/'+id,
                method:'put',
                data: JSON.stringify(item),
                contentType:'application/json',
                success: function(data){  
                    alert('field updated successful'); 
                    console.log(data) ;
                },
                error: function(error){
                    console.log(error);
                }
            });      
       
        });
    },

    deleteData: function(){
        
        $('table').on('click','.btn-danger', function(){
            var rowel = $(this).closest('tr');
            var id = rowel.find(".id").text();
            console.log(id);
        
            $.ajax({
                url:'/api/'+id,
                method:'delete',
                contentType:'application/json',
                success: realTime,
                error: function(error){
                    console.log(error);
                }
            });
            
            function realTime(data){
                $('tbody td').hide();
                window.location.href= "index.html";
            }
        
        });
    },

    process: function(){
        this.getData();
        this.saveData();
        this.updateData();
        this.deleteData();
    }
}

app.process();
